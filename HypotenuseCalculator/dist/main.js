/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

function lineHelper(line, x1, y1, x2, y2) { //helper, does not need to be exported
    line.setAttribute("x1", x1);
    line.setAttribute("y1", y1);
    line.setAttribute("x2", x2);
    line.setAttribute("y2", y2);
    //Note this function returns undefined implicitly
    //We don't need a return value from it, we just need it
    //to change the line attributes.
}
function textHelper(textElement, x, y, text) {
    textElement.setAttribute("x", x);
    textElement.setAttribute("y", y);
    textElement.innerHTML = text;
}


function calculate() {  //event listener
    //Input
    let inputa = document.querySelector("#a");
    let inputb = document.querySelector("#b");
    let a = Number(inputa.value);  //Data types matter, so we need to convert a to a number. Comes in as a string
    let b = Number(inputb.value);

    //Processing(calculation)
    let c = Math.sqrt(a * a + b * b);
    let cFormatted = c.toFixed(2);
    console.log("c:", cFormatted);  //for debugging, to make sure we did the calculation correctly

    //adjust lines (output, but complicated output)
    let scale = 20;  //scale the numbers, so it's visible in the drawing
    x0 = 100;
    y0 = 100;
    x = 100 + a * scale;
    y = 100 + b * scale;

    //adjust lines (We need to do some math, but this math is not part of the core hypotenuse calculation)
    //Note we could have used querySelectorAll and arrays to shorten the code, but more on that next week
    let line1 = document.querySelector("#line1");
    let line2 = document.querySelector("#line2");
    let line3 = document.querySelector("#line3");
    lineHelper(line1, x0, y0, x, y0);
    lineHelper(line2, x0, y0, x0, y);
    lineHelper(line3, x0, y, x, y0);

    //Adjust text placement and value
    let texta = document.querySelector("#texta");
    let textb = document.querySelector("#textb");
    let textc = document.querySelector("#textc");
    textHelper(texta, (x0 + x) / 2, y0, "a:" + a);
    textHelper(textb, x0, (y0 + y) / 2, "b:" + b);
    textHelper(textc, (x0 + x) / 2, (y0 + y) / 2, "c:" + cFormatted);

}
//exports
window.calculate = calculate;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7O0FBRUEsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQzs7QUFFbEM7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNkIiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuXHJcbmZ1bmN0aW9uIGxpbmVIZWxwZXIobGluZSwgeDEsIHkxLCB4MiwgeTIpIHsgLy9oZWxwZXIsIGRvZXMgbm90IG5lZWQgdG8gYmUgZXhwb3J0ZWRcclxuICAgIGxpbmUuc2V0QXR0cmlidXRlKFwieDFcIiwgeDEpO1xyXG4gICAgbGluZS5zZXRBdHRyaWJ1dGUoXCJ5MVwiLCB5MSk7XHJcbiAgICBsaW5lLnNldEF0dHJpYnV0ZShcIngyXCIsIHgyKTtcclxuICAgIGxpbmUuc2V0QXR0cmlidXRlKFwieTJcIiwgeTIpO1xyXG4gICAgLy9Ob3RlIHRoaXMgZnVuY3Rpb24gcmV0dXJucyB1bmRlZmluZWQgaW1wbGljaXRseVxyXG4gICAgLy9XZSBkb24ndCBuZWVkIGEgcmV0dXJuIHZhbHVlIGZyb20gaXQsIHdlIGp1c3QgbmVlZCBpdFxyXG4gICAgLy90byBjaGFuZ2UgdGhlIGxpbmUgYXR0cmlidXRlcy5cclxufVxyXG5mdW5jdGlvbiB0ZXh0SGVscGVyKHRleHRFbGVtZW50LCB4LCB5LCB0ZXh0KSB7XHJcbiAgICB0ZXh0RWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJ4XCIsIHgpO1xyXG4gICAgdGV4dEVsZW1lbnQuc2V0QXR0cmlidXRlKFwieVwiLCB5KTtcclxuICAgIHRleHRFbGVtZW50LmlubmVySFRNTCA9IHRleHQ7XHJcbn1cclxuXHJcblxyXG5mdW5jdGlvbiBjYWxjdWxhdGUoKSB7ICAvL2V2ZW50IGxpc3RlbmVyXHJcbiAgICAvL0lucHV0XHJcbiAgICBsZXQgaW5wdXRhID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNhXCIpO1xyXG4gICAgbGV0IGlucHV0YiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjYlwiKTtcclxuICAgIGxldCBhID0gTnVtYmVyKGlucHV0YS52YWx1ZSk7ICAvL0RhdGEgdHlwZXMgbWF0dGVyLCBzbyB3ZSBuZWVkIHRvIGNvbnZlcnQgYSB0byBhIG51bWJlci4gQ29tZXMgaW4gYXMgYSBzdHJpbmdcclxuICAgIGxldCBiID0gTnVtYmVyKGlucHV0Yi52YWx1ZSk7XHJcblxyXG4gICAgLy9Qcm9jZXNzaW5nKGNhbGN1bGF0aW9uKVxyXG4gICAgbGV0IGMgPSBNYXRoLnNxcnQoYSAqIGEgKyBiICogYik7XHJcbiAgICBsZXQgY0Zvcm1hdHRlZCA9IGMudG9GaXhlZCgyKTtcclxuICAgIGNvbnNvbGUubG9nKFwiYzpcIiwgY0Zvcm1hdHRlZCk7ICAvL2ZvciBkZWJ1Z2dpbmcsIHRvIG1ha2Ugc3VyZSB3ZSBkaWQgdGhlIGNhbGN1bGF0aW9uIGNvcnJlY3RseVxyXG5cclxuICAgIC8vYWRqdXN0IGxpbmVzIChvdXRwdXQsIGJ1dCBjb21wbGljYXRlZCBvdXRwdXQpXHJcbiAgICBsZXQgc2NhbGUgPSAyMDsgIC8vc2NhbGUgdGhlIG51bWJlcnMsIHNvIGl0J3MgdmlzaWJsZSBpbiB0aGUgZHJhd2luZ1xyXG4gICAgeDAgPSAxMDA7XHJcbiAgICB5MCA9IDEwMDtcclxuICAgIHggPSAxMDAgKyBhICogc2NhbGU7XHJcbiAgICB5ID0gMTAwICsgYiAqIHNjYWxlO1xyXG5cclxuICAgIC8vYWRqdXN0IGxpbmVzIChXZSBuZWVkIHRvIGRvIHNvbWUgbWF0aCwgYnV0IHRoaXMgbWF0aCBpcyBub3QgcGFydCBvZiB0aGUgY29yZSBoeXBvdGVudXNlIGNhbGN1bGF0aW9uKVxyXG4gICAgLy9Ob3RlIHdlIGNvdWxkIGhhdmUgdXNlZCBxdWVyeVNlbGVjdG9yQWxsIGFuZCBhcnJheXMgdG8gc2hvcnRlbiB0aGUgY29kZSwgYnV0IG1vcmUgb24gdGhhdCBuZXh0IHdlZWtcclxuICAgIGxldCBsaW5lMSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbGluZTFcIik7XHJcbiAgICBsZXQgbGluZTIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2xpbmUyXCIpO1xyXG4gICAgbGV0IGxpbmUzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNsaW5lM1wiKTtcclxuICAgIGxpbmVIZWxwZXIobGluZTEsIHgwLCB5MCwgeCwgeTApO1xyXG4gICAgbGluZUhlbHBlcihsaW5lMiwgeDAsIHkwLCB4MCwgeSk7XHJcbiAgICBsaW5lSGVscGVyKGxpbmUzLCB4MCwgeSwgeCwgeTApO1xyXG5cclxuICAgIC8vQWRqdXN0IHRleHQgcGxhY2VtZW50IGFuZCB2YWx1ZVxyXG4gICAgbGV0IHRleHRhID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN0ZXh0YVwiKTtcclxuICAgIGxldCB0ZXh0YiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjdGV4dGJcIik7XHJcbiAgICBsZXQgdGV4dGMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3RleHRjXCIpO1xyXG4gICAgdGV4dEhlbHBlcih0ZXh0YSwgKHgwICsgeCkgLyAyLCB5MCwgXCJhOlwiICsgYSk7XHJcbiAgICB0ZXh0SGVscGVyKHRleHRiLCB4MCwgKHkwICsgeSkgLyAyLCBcImI6XCIgKyBiKTtcclxuICAgIHRleHRIZWxwZXIodGV4dGMsICh4MCArIHgpIC8gMiwgKHkwICsgeSkgLyAyLCBcImM6XCIgKyBjRm9ybWF0dGVkKTtcclxuXHJcbn1cclxuLy9leHBvcnRzXHJcbndpbmRvdy5jYWxjdWxhdGUgPSBjYWxjdWxhdGU7Il0sInNvdXJjZVJvb3QiOiIifQ==