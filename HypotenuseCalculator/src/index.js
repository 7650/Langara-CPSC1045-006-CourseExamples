//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

function lineHelper(line, x1, y1, x2, y2) { //helper, does not need to be exported
    line.setAttribute("x1", x1);
    line.setAttribute("y1", y1);
    line.setAttribute("x2", x2);
    line.setAttribute("y2", y2);
    //Note this function returns undefined implicitly
    //We don't need a return value from it, we just need it
    //to change the line attributes.
}
function textHelper(textElement, x, y, text) {
    textElement.setAttribute("x", x);
    textElement.setAttribute("y", y);
    textElement.innerHTML = text;
}


function calculate() {  //event listener
    //Input
    let inputa = document.querySelector("#a");
    let inputb = document.querySelector("#b");
    let a = Number(inputa.value);  //Data types matter, so we need to convert a to a number. Comes in as a string
    let b = Number(inputb.value);

    //Processing(calculation)
    let c = Math.sqrt(a * a + b * b);
    let cFormatted = c.toFixed(2);
    console.log("c:", cFormatted);  //for debugging, to make sure we did the calculation correctly

    //adjust lines (output, but complicated output)
    let scale = 20;  //scale the numbers, so it's visible in the drawing
    x0 = 100;
    y0 = 100;
    x = 100 + a * scale;
    y = 100 + b * scale;

    //adjust lines (We need to do some math, but this math is not part of the core hypotenuse calculation)
    //Note we could have used querySelectorAll and arrays to shorten the code, but more on that next week
    let line1 = document.querySelector("#line1");
    let line2 = document.querySelector("#line2");
    let line3 = document.querySelector("#line3");
    lineHelper(line1, x0, y0, x, y0);
    lineHelper(line2, x0, y0, x0, y);
    lineHelper(line3, x0, y, x, y0);

    //Adjust text placement and value
    let texta = document.querySelector("#texta");
    let textb = document.querySelector("#textb");
    let textc = document.querySelector("#textc");
    textHelper(texta, (x0 + x) / 2, y0, "a:" + a);
    textHelper(textb, x0, (y0 + y) / 2, "b:" + b);
    textHelper(textc, (x0 + x) / 2, (y0 + y) / 2, "c:" + cFormatted);

}
//exports
window.calculate = calculate;